# Imposture affinitaire et empathie exploitable

[Original zine in English](https://gitlab.com/hakan-geijer/affinity-fraud)

Ce zine explore comment l'identité peut offrir une couverture à des disruptions intentionnelles ou accidentelles dans les cercles radicaux et le militantisme, et comment la Culture de la Sécurité que nous avons développé pour atténuer beaucoup de menaces peut entrer en conflit avec les normes anti-racistes, anti-sexistes et généralement progressistes à l'intérieur de nos mouvements.

## (Anti-)Copyright

This zine and repo are public domain under a CC0 license.
